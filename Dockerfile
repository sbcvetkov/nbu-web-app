FROM nginx:alpine

RUN apk update && apk add wget
COPY web-app /usr/share/nginx/html
COPY locations.csv /usr/share/nginx/html/data