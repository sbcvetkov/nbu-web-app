pipeline {

    triggers {
        cron('0 0 * * * ')
    }

    environment {
        registry = "sbcvetkov/nbu-web-app"
        registryCredential = 'dockerhub-creds'
        dockerImage = ''
    }

    agent { label 'docker-slave' }

    stages {
        stage('Cloning Git') {
            steps {
                checkout scm
            }
        }

        stage('Building image') {
            steps {
                script {
                    sh 'wget https://covid.ourworldindata.org/data/ecdc/locations.csv'
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                    sh "docker tag ${registry}:${BUILD_NUMBER} ${registry}:latest"
                }
            }
        }
    
        stage('Test image') {
            steps {
                script {
                    sh 'docker run -d --name nbu-webapp-test -p 8090:80 sbcvetkov/nbu-web-app:' + "$BUILD_NUMBER"
                    def response = httpRequest 'http://192.168.77.10:8090/index.html'
                    if (response.status == 200) {
                        println("Status: "+ response.status)
                        println("Test successful!")
                    } else {
                        println("Status: "+ response.status)
                        println("Test failed! Unable to reach the webapp...")
                        currentBuild.result = 'FAILED'
                        return
                    }
                } 
            }
        }

        stage('Deploy Image') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }
    }

    post {

        always {
            script {
                sh '''#!/bin/bash
                    if docker ps -a --format '{{.Names}}' | grep -Eq "nbu-webapp-test"; then
                        docker stop nbu-webapp-test && docker rm nbu-webapp-test
                        echo 'nbu-webapp-test container removed'
                    fi
                    '''              
            }
        }

        success {
            script {
                sh '''#!/bin/bash
                    if docker ps -a --format '{{.Names}}' | grep -Eq "nbu-webapp"; then
                        docker stop nbu-webapp && docker rm nbu-webapp
                        echo 'nbu-webapp container removed'
                    fi
                    '''
                sh 'docker run -d --name nbu-webapp -p 8080:80 sbcvetkov/nbu-web-app:' + "$BUILD_NUMBER"
            }
        }

        failure {
            script {
                sh "docker rmi $registry:$BUILD_NUMBER"  
            }
        }
    }
}
